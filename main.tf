provider "aws" {
    region = "ap-southeast-2"

}

variable "subnet_cidr_block" {
    description = "sbunet cidr block"
  
}

variable "vpc_cidr_block" {
    description = "vpc codr block"
}

variable "environment" {
    description = "deployment environment"
}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = var.environment
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.subnet_cidr_block 
    availability_zone = "ap-southeast-2a"
    tags = {
        Name = "subnet-1-dev"
    }
}

output "dev-vpc-id" {
    value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}